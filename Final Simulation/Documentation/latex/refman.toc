\contentsline {chapter}{\numberline {1}\discretionary {-}{}{}Class \discretionary {-}{}{}Index}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}\discretionary {-}{}{}Class \discretionary {-}{}{}List}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}\discretionary {-}{}{}File \discretionary {-}{}{}Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}\discretionary {-}{}{}File \discretionary {-}{}{}List}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}\discretionary {-}{}{}Class \discretionary {-}{}{}Documentation}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}course \discretionary {-}{}{}Struct \discretionary {-}{}{}Reference}{5}{section.3.1}
\contentsline {section}{\numberline {3.2}dept \discretionary {-}{}{}Struct \discretionary {-}{}{}Reference}{5}{section.3.2}
\contentsline {section}{\numberline {3.3}facnode \discretionary {-}{}{}Struct \discretionary {-}{}{}Reference}{6}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{6}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}\discretionary {-}{}{}Member \discretionary {-}{}{}Data \discretionary {-}{}{}Documentation}{6}{subsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.2.1}courses}{6}{subsubsection.3.3.2.1}
\contentsline {subsubsection}{\numberline {3.3.2.2}dept}{6}{subsubsection.3.3.2.2}
\contentsline {subsubsection}{\numberline {3.3.2.3}entryno}{6}{subsubsection.3.3.2.3}
\contentsline {subsubsection}{\numberline {3.3.2.4}hostel}{7}{subsubsection.3.3.2.4}
\contentsline {subsubsection}{\numberline {3.3.2.5}interests}{7}{subsubsection.3.3.2.5}
\contentsline {subsubsection}{\numberline {3.3.2.6}name}{7}{subsubsection.3.3.2.6}
\contentsline {subsubsection}{\numberline {3.3.2.7}surname}{7}{subsubsection.3.3.2.7}
\contentsline {subsubsection}{\numberline {3.3.2.8}univ}{7}{subsubsection.3.3.2.8}
\contentsline {section}{\numberline {3.4}faculty \discretionary {-}{}{}Struct \discretionary {-}{}{}Reference}{7}{section.3.4}
\contentsline {section}{\numberline {3.5}graph \discretionary {-}{}{}Class \discretionary {-}{}{}Reference}{8}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{8}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}\discretionary {-}{}{}Member \discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{8}{subsection.3.5.2}
\contentsline {subsubsection}{\numberline {3.5.2.1}already\discretionary {-}{}{}\_\discretionary {-}{}{}friends}{8}{subsubsection.3.5.2.1}
\contentsline {subsubsection}{\numberline {3.5.2.2}create\discretionary {-}{}{}\_\discretionary {-}{}{}edge}{9}{subsubsection.3.5.2.2}
\contentsline {subsubsection}{\numberline {3.5.2.3}del\discretionary {-}{}{}\_\discretionary {-}{}{}edge}{9}{subsubsection.3.5.2.3}
\contentsline {subsubsection}{\numberline {3.5.2.4}list\discretionary {-}{}{}\_\discretionary {-}{}{}of\discretionary {-}{}{}\_\discretionary {-}{}{}friends}{9}{subsubsection.3.5.2.4}
\contentsline {subsubsection}{\numberline {3.5.2.5}no\discretionary {-}{}{}\_\discretionary {-}{}{}of\discretionary {-}{}{}\_\discretionary {-}{}{}friends}{10}{subsubsection.3.5.2.5}
\contentsline {subsection}{\numberline {3.5.3}\discretionary {-}{}{}Member \discretionary {-}{}{}Data \discretionary {-}{}{}Documentation}{10}{subsection.3.5.3}
\contentsline {subsubsection}{\numberline {3.5.3.1}numedges}{10}{subsubsection.3.5.3.1}
\contentsline {subsubsection}{\numberline {3.5.3.2}numvertices}{10}{subsubsection.3.5.3.2}
\contentsline {section}{\numberline {3.6}hostels \discretionary {-}{}{}Struct \discretionary {-}{}{}Reference}{10}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{11}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}\discretionary {-}{}{}Member \discretionary {-}{}{}Data \discretionary {-}{}{}Documentation}{11}{subsection.3.6.2}
\contentsline {subsubsection}{\numberline {3.6.2.1}hostel\discretionary {-}{}{}\_\discretionary {-}{}{}stud}{11}{subsubsection.3.6.2.1}
\contentsline {subsubsection}{\numberline {3.6.2.2}name}{11}{subsubsection.3.6.2.2}
\contentsline {subsubsection}{\numberline {3.6.2.3}we}{11}{subsubsection.3.6.2.3}
\contentsline {section}{\numberline {3.7}houses \discretionary {-}{}{}Struct \discretionary {-}{}{}Reference}{11}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{11}{subsection.3.7.1}
\contentsline {subsection}{\numberline {3.7.2}\discretionary {-}{}{}Member \discretionary {-}{}{}Data \discretionary {-}{}{}Documentation}{11}{subsection.3.7.2}
\contentsline {subsubsection}{\numberline {3.7.2.1}fill}{11}{subsubsection.3.7.2.1}
\contentsline {subsubsection}{\numberline {3.7.2.2}house\discretionary {-}{}{}\_\discretionary {-}{}{}fac}{12}{subsubsection.3.7.2.2}
\contentsline {subsubsection}{\numberline {3.7.2.3}name}{12}{subsubsection.3.7.2.3}
\contentsline {section}{\numberline {3.8}interest \discretionary {-}{}{}Struct \discretionary {-}{}{}Reference}{12}{section.3.8}
\contentsline {subsection}{\numberline {3.8.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{12}{subsection.3.8.1}
\contentsline {subsection}{\numberline {3.8.2}\discretionary {-}{}{}Member \discretionary {-}{}{}Data \discretionary {-}{}{}Documentation}{12}{subsection.3.8.2}
\contentsline {subsubsection}{\numberline {3.8.2.1}interest\discretionary {-}{}{}\_\discretionary {-}{}{}fac}{12}{subsubsection.3.8.2.1}
\contentsline {subsubsection}{\numberline {3.8.2.2}interest\discretionary {-}{}{}\_\discretionary {-}{}{}stud}{12}{subsubsection.3.8.2.2}
\contentsline {subsubsection}{\numberline {3.8.2.3}name}{12}{subsubsection.3.8.2.3}
\contentsline {section}{\numberline {3.9}mymsgbuf \discretionary {-}{}{}Struct \discretionary {-}{}{}Reference}{13}{section.3.9}
\contentsline {subsection}{\numberline {3.9.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{13}{subsection.3.9.1}
\contentsline {section}{\numberline {3.10}pq\discretionary {-}{}{}\_\discretionary {-}{}{}operand \discretionary {-}{}{}Class \discretionary {-}{}{}Reference}{13}{section.3.10}
\contentsline {subsection}{\numberline {3.10.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{13}{subsection.3.10.1}
\contentsline {subsection}{\numberline {3.10.2}\discretionary {-}{}{}Member \discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{13}{subsection.3.10.2}
\contentsline {subsubsection}{\numberline {3.10.2.1}operator()}{13}{subsubsection.3.10.2.1}
\contentsline {section}{\numberline {3.11}student \discretionary {-}{}{}Struct \discretionary {-}{}{}Reference}{14}{section.3.11}
\contentsline {section}{\numberline {3.12}studnode \discretionary {-}{}{}Struct \discretionary {-}{}{}Reference}{14}{section.3.12}
\contentsline {subsection}{\numberline {3.12.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{15}{subsection.3.12.1}
\contentsline {subsection}{\numberline {3.12.2}\discretionary {-}{}{}Member \discretionary {-}{}{}Data \discretionary {-}{}{}Documentation}{15}{subsection.3.12.2}
\contentsline {subsubsection}{\numberline {3.12.2.1}batch}{15}{subsubsection.3.12.2.1}
\contentsline {subsubsection}{\numberline {3.12.2.2}courses}{15}{subsubsection.3.12.2.2}
\contentsline {subsubsection}{\numberline {3.12.2.3}dept}{15}{subsubsection.3.12.2.3}
\contentsline {subsubsection}{\numberline {3.12.2.4}entryno}{15}{subsubsection.3.12.2.4}
\contentsline {subsubsection}{\numberline {3.12.2.5}floor}{15}{subsubsection.3.12.2.5}
\contentsline {subsubsection}{\numberline {3.12.2.6}friends}{15}{subsubsection.3.12.2.6}
\contentsline {subsubsection}{\numberline {3.12.2.7}hostel}{15}{subsubsection.3.12.2.7}
\contentsline {subsubsection}{\numberline {3.12.2.8}name}{16}{subsubsection.3.12.2.8}
\contentsline {subsubsection}{\numberline {3.12.2.9}room\discretionary {-}{}{}No}{16}{subsubsection.3.12.2.9}
\contentsline {subsubsection}{\numberline {3.12.2.10}surname}{16}{subsubsection.3.12.2.10}
\contentsline {subsubsection}{\numberline {3.12.2.11}univ}{16}{subsubsection.3.12.2.11}
\contentsline {subsubsection}{\numberline {3.12.2.12}year}{16}{subsubsection.3.12.2.12}
\contentsline {section}{\numberline {3.13}\discretionary {-}{}{}U\discretionary {-}{}{}N\discretionary {-}{}{}I\discretionary {-}{}{}V \discretionary {-}{}{}Struct \discretionary {-}{}{}Reference}{16}{section.3.13}
\contentsline {chapter}{\numberline {4}\discretionary {-}{}{}File \discretionary {-}{}{}Documentation}{17}{chapter.4}
\contentsline {section}{\numberline {4.1}analyser.\discretionary {-}{}{}h \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{17}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{18}{subsection.4.1.1}
\contentsline {section}{\numberline {4.2}gen\discretionary {-}{}{}Course.\discretionary {-}{}{}h \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{18}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{18}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}\discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{18}{subsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.2.1}allocate\discretionary {-}{}{}Courses}{18}{subsubsection.4.2.2.1}
\contentsline {subsubsection}{\numberline {4.2.2.2}float\discretionary {-}{}{}Courses}{18}{subsubsection.4.2.2.2}
\contentsline {section}{\numberline {4.3}generator.\discretionary {-}{}{}h \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{19}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{19}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}\discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{19}{subsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.2.1}generate\discretionary {-}{}{}Courses}{19}{subsubsection.4.3.2.1}
\contentsline {subsubsection}{\numberline {4.3.2.2}generate\discretionary {-}{}{}Faculty}{20}{subsubsection.4.3.2.2}
\contentsline {subsubsection}{\numberline {4.3.2.3}generate\discretionary {-}{}{}Students}{20}{subsubsection.4.3.2.3}
\contentsline {subsubsection}{\numberline {4.3.2.4}gen\discretionary {-}{}{}Fen}{20}{subsubsection.4.3.2.4}
\contentsline {subsubsection}{\numberline {4.3.2.5}readpp}{21}{subsubsection.4.3.2.5}
\contentsline {section}{\numberline {4.4}genfac.\discretionary {-}{}{}h \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{21}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{21}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}\discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{21}{subsection.4.4.2}
\contentsline {subsubsection}{\numberline {4.4.2.1}genfacof1dept}{21}{subsubsection.4.4.2.1}
\contentsline {subsubsection}{\numberline {4.4.2.2}randominterest\discretionary {-}{}{}\_\discretionary {-}{}{}fac}{22}{subsubsection.4.4.2.2}
\contentsline {section}{\numberline {4.5}gen\discretionary {-}{}{}Friend.\discretionary {-}{}{}h \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{22}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{22}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}\discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{22}{subsection.4.5.2}
\contentsline {subsubsection}{\numberline {4.5.2.1}gen\discretionary {-}{}{}Friends}{22}{subsubsection.4.5.2.1}
\contentsline {section}{\numberline {4.6}genstudent.\discretionary {-}{}{}h \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{23}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{23}{subsection.4.6.1}
\contentsline {subsection}{\numberline {4.6.2}\discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{23}{subsection.4.6.2}
\contentsline {subsubsection}{\numberline {4.6.2.1}genstuof1dept}{23}{subsubsection.4.6.2.1}
\contentsline {subsubsection}{\numberline {4.6.2.2}increase\discretionary {-}{}{}\_\discretionary {-}{}{}year}{24}{subsubsection.4.6.2.2}
\contentsline {subsubsection}{\numberline {4.6.2.3}kill}{24}{subsubsection.4.6.2.3}
\contentsline {subsubsection}{\numberline {4.6.2.4}kill\discretionary {-}{}{}\_\discretionary {-}{}{}all}{24}{subsubsection.4.6.2.4}
\contentsline {subsubsection}{\numberline {4.6.2.5}randominterest\discretionary {-}{}{}\_\discretionary {-}{}{}st}{24}{subsubsection.4.6.2.5}
\contentsline {section}{\numberline {4.7}global1.\discretionary {-}{}{}h \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{25}{section.4.7}
\contentsline {subsection}{\numberline {4.7.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{26}{subsection.4.7.1}
\contentsline {subsection}{\numberline {4.7.2}\discretionary {-}{}{}Variable \discretionary {-}{}{}Documentation}{27}{subsection.4.7.2}
\contentsline {subsubsection}{\numberline {4.7.2.1}allinterest}{27}{subsubsection.4.7.2.1}
\contentsline {subsubsection}{\numberline {4.7.2.2}allstudents}{27}{subsubsection.4.7.2.2}
\contentsline {subsubsection}{\numberline {4.7.2.3}entryno}{27}{subsubsection.4.7.2.3}
\contentsline {subsubsection}{\numberline {4.7.2.4}fac\discretionary {-}{}{}Cond}{27}{subsubsection.4.7.2.4}
\contentsline {subsubsection}{\numberline {4.7.2.5}fac\discretionary {-}{}{}Mutex}{27}{subsubsection.4.7.2.5}
\contentsline {subsubsection}{\numberline {4.7.2.6}fac\discretionary {-}{}{}R\discretionary {-}{}{}A\discretionary {-}{}{}N\discretionary {-}{}{}D\discretionary {-}{}{}O\discretionary {-}{}{}M}{27}{subsubsection.4.7.2.6}
\contentsline {subsubsection}{\numberline {4.7.2.7}gentotime}{27}{subsubsection.4.7.2.7}
\contentsline {subsubsection}{\numberline {4.7.2.8}prof\discretionary {-}{}{}\_\discretionary {-}{}{}name}{27}{subsubsection.4.7.2.8}
\contentsline {subsubsection}{\numberline {4.7.2.9}run\discretionary {-}{}{}Thd\discretionary {-}{}{}Stud}{28}{subsubsection.4.7.2.9}
\contentsline {subsubsection}{\numberline {4.7.2.10}stud\discretionary {-}{}{}\_\discretionary {-}{}{}name}{28}{subsubsection.4.7.2.10}
\contentsline {section}{\numberline {4.8}graphml.\discretionary {-}{}{}h \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{28}{section.4.8}
\contentsline {subsection}{\numberline {4.8.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{28}{subsection.4.8.1}
\contentsline {subsection}{\numberline {4.8.2}\discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{28}{subsection.4.8.2}
\contentsline {subsubsection}{\numberline {4.8.2.1}graphconverter}{28}{subsubsection.4.8.2.1}
\contentsline {section}{\numberline {4.9}main\discretionary {-}{}{}\_\discretionary {-}{}{}2.\discretionary {-}{}{}h \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{28}{section.4.9}
\contentsline {subsection}{\numberline {4.9.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{29}{subsection.4.9.1}
\contentsline {subsection}{\numberline {4.9.2}\discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{29}{subsection.4.9.2}
\contentsline {subsubsection}{\numberline {4.9.2.1}set\discretionary {-}{}{}Enviro}{29}{subsubsection.4.9.2.1}
\contentsline {section}{\numberline {4.10}message\discretionary {-}{}{}\_\discretionary {-}{}{}temp.\discretionary {-}{}{}h \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{29}{section.4.10}
\contentsline {subsection}{\numberline {4.10.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{30}{subsection.4.10.1}
\contentsline {subsection}{\numberline {4.10.2}\discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{30}{subsection.4.10.2}
\contentsline {subsubsection}{\numberline {4.10.2.1}open\discretionary {-}{}{}\_\discretionary {-}{}{}queue}{30}{subsubsection.4.10.2.1}
\contentsline {subsubsection}{\numberline {4.10.2.2}rec\discretionary {-}{}{}\_\discretionary {-}{}{}msg}{30}{subsubsection.4.10.2.2}
\contentsline {subsubsection}{\numberline {4.10.2.3}send\discretionary {-}{}{}\_\discretionary {-}{}{}msg}{30}{subsubsection.4.10.2.3}
\contentsline {section}{\numberline {4.11}\discretionary {-}{}{}Semaphore.\discretionary {-}{}{}h \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{31}{section.4.11}
\contentsline {subsection}{\numberline {4.11.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{31}{subsection.4.11.1}
\contentsline {subsection}{\numberline {4.11.2}\discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{31}{subsection.4.11.2}
\contentsline {subsubsection}{\numberline {4.11.2.1}get\discretionary {-}{}{}Sem}{31}{subsubsection.4.11.2.1}
\contentsline {subsubsection}{\numberline {4.11.2.2}open\discretionary {-}{}{}\_\discretionary {-}{}{}sem}{31}{subsubsection.4.11.2.2}
\contentsline {subsubsection}{\numberline {4.11.2.3}rel\discretionary {-}{}{}Sem}{32}{subsubsection.4.11.2.3}
